const assert = require('assert');
const isPrime = require('../utils/primes').isPrime;

describe('isPrime', function() {
  it('should tell 5 is a prime number', function() {
    assert.equal(isPrime(5), true);
  });

  it('should tell 27 is not a prime number', function() {
    assert.equal(isPrime(27), false);
  });
});
